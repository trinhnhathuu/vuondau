const express = require('express');
const router = express.Router();
const db = require('../models/db');

router.get('/vuondau', (req, res) => {

  const sql = 'SELECT * FROM vuon_dau';
  
  db.query(sql, (err, result) => {
    if (err) throw err;
    
    res.json(result); 
  });

});

module.exports = router;