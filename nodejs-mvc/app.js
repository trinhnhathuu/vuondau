// app.js

const express = require('express')
const app = express()
const cors = require('cors');
const vuonrauRoutes = require('./controllers/vuonrau');


app.set('view engine', 'ejs')
app.use(cors());
app.use(require('./controllers/mainController'))
app.use('/api', vuonrauRoutes);

app.listen(8888)
